/**
 * COPYRIGHT (C) 2013 Corvusoft. All Rights Reserved.
 */

import org.webrtc.DataChannel;

public class DataChannelObserver implements DataChannel.Observer {
    @Override
    public synchronized void onStateChange() {
        System.out.println("DataChannelObserver: onStateChange called.");
    }

    @Override
    public synchronized void onMessage(DataChannel.Buffer buffer) {
        System.out.println("DataChannelObserver: onMessage called with message: " + buffer);
    }
}
