/**
 * COPYRIGHT (C) 2013 Corvusoft. All Rights Reserved.
 */
import java.util.List;
import java.util.ArrayList;

import org.webrtc.AudioTrack;
import org.webrtc.VideoTrack;
import org.webrtc.VideoSource;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoRenderer;

import org.webrtc.DataChannel;
import org.webrtc.MediaStream;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;

import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.PeerConnection.Observer;
import org.webrtc.PeerConnection.IceServer;
import org.webrtc.PeerConnection.SignalingState;
import org.webrtc.PeerConnection.IceGatheringState;
import org.webrtc.PeerConnection.IceConnectionState;

/**
 * This class implements an abstract interface for an observer. It is up to this class to
 * implement a derived class which implements the observer class. The observer is registered when the PeerConnection
 * is created using the PeerConnectionFactoryInterface.
 */
public class PeerConnectionObserver implements Observer {
    private final PeerConnectionFactory factory = new PeerConnectionFactory();

    private MediaStream media_stream = null;

    private List<IceServer> ice_servers = null;

    private PeerConnection peer_connection = null;

    private VideoRenderer video_render = null;

    private DataChannel data_channel = null;

    public PeerConnectionObserver() {
        setup();
    }

    void setup() {
        setup_ice_servers();
        setup_media_streams();
        setup_peer_connection();
        setup_data_channel();
        setup_sdp_observer();
    }

    void setup_ice_servers() {
        ice_servers = new ArrayList();
        ice_servers.add(new IceServer("stun:stun.l.google.com:19302"));
    }

    void setup_media_streams() {
        media_stream = factory.createLocalMediaStream("Local Media Stream Label");

        setup_video_renderer();
        setup_video_stream();
        setup_audio_stream();
    }

    void setup_video_renderer() {
        //video_render = new VideoRenderer(new VideoRender());
        video_render = VideoRenderer.createGui(500, 500);
    }

    void setup_video_stream() {
        MediaConstraints media_constraints = new MediaConstraints();

        VideoCapturer video_capturer = VideoCapturer.create("");

        VideoSource video_source = factory.createVideoSource(video_capturer, media_constraints);

        VideoTrack video_track = factory.createVideoTrack("Local Video Track Label", video_source);

        video_track.addRenderer(video_render);

        media_stream.addTrack(video_track);
    }

    void setup_audio_stream() {
        AudioTrack audio_track = factory.createAudioTrack("Local Audio Track Label");

        media_stream.addTrack(audio_track);
    }

    void setup_peer_connection() {
        MediaConstraints media_constraints = new MediaConstraints();
        media_constraints.mandatory.add(new MediaConstraints.KeyValuePair("DtlsSrtpKeyAgreement", "true"));
        media_constraints.optional.add(new MediaConstraints.KeyValuePair("RtpDataChannels", "true"));

        peer_connection = factory.createPeerConnection(ice_servers, media_constraints, this);
        peer_connection.addStream(media_stream, new MediaConstraints());
    }

    void setup_data_channel() {
        data_channel = peer_connection.createDataChannel("Local Data Channel", new DataChannel.Init());

        data_channel.registerObserver(new DataChannelObserver());
    }

    void setup_sdp_observer() {
        MediaConstraints media_constraints = new MediaConstraints();

        SDPObserver sdp_observer = new SDPObserver();

        peer_connection.createOffer(sdp_observer, media_constraints);
    }

    @Override
    public synchronized void onSignalingChange(SignalingState signaling_state) {
        System.out.println("PeerConnectionObserver: onSignalingChange called with state: " + signaling_state.name());

        switch(signaling_state) {
            case HAVE_LOCAL_OFFER:
                break;
            case HAVE_LOCAL_PRANSWER:
                break;
            case HAVE_REMOTE_OFFER:
                break;
            case HAVE_REMOTE_PRANSWER:
                break;
            default:
                //unknown state error!
                break;
        }
    }

    @Override
    public synchronized void onAddStream(MediaStream media_stream) {
        System.out.println("PeerConnectionObserver: onAddStream called with media stream label: " + media_stream.label());
    }

    @Override
    public synchronized void onRemoveStream(MediaStream mediaStream) {
        System.out.println("PeerConnectionObserver: onRemoveStream called with media stream label: " + media_stream.label());
    }

    @Override
    public synchronized void onError() {
        System.out.println("PeerConnectionObserver: onError called.");
    }

    @Override
    public synchronized void onDataChannel(DataChannel data_channel) {
        System.out.println("PeerConnectionObserver: onDataChannel called with data channel label: " + data_channel.label());
    }

    @Override
    public synchronized void onIceCandidate(IceCandidate ice_candidate) {
        System.out.println("PeerConnectionObserver: onIceCandidate called.");
    }

    @Override
    public synchronized void onIceGatheringChange(IceGatheringState ice_gathering_state) {
        System.out.println("PeerConnectionObserver: onIceGatheringChange called with state: " + ice_gathering_state.name());
    }

    @Override
    public synchronized void onIceConnectionChange(IceConnectionState ice_connection_state) {
        System.out.println("PeerConnectionObserver: onIceConnectionChange called with state: " + ice_connection_state.name());
    }
}
