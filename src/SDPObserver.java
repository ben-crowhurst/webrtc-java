/**
 * COPYRIGHT (C) 2013 Corvusoft. All Rights Reserved.
 */

import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;

public class SDPObserver implements SdpObserver {
    @Override
    public void onCreateFailure(String failure) {
        System.out.println("SDPObserver: onCreateFailure called with failure: " + failure);
    }

    @Override
    public void onCreateSuccess(SessionDescription session_description) {
        System.out.println("SDPObserver: onCreateSuccess called with description: " + session_description);
    }

    @Override
    public void onSetFailure(String failure) {
        System.out.println("SDPObserver: onSetFailure called with failure: " + failure);
    }

    @Override
    public void onSetSuccess() {
        System.out.println("SDPObserver: onSetSuccess called.");
    }
}
